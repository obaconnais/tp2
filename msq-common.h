/**
  * Fichier msq-common.h
*/
#ifndef MSQ_COMMON_H
#define MSQ_COMMON_H

#include <stdio.h>       /* Bibliotheque standard.                */
#include <string.h>      /* Bibliotheque standard.                */
#include <stdlib.h>      /* Bibliotheque standard pour strtol.    */
#include <getopt.h>      /* Bibliothèque pour saisie des options. */
#include <sys/msg.h>     /* Bibliothèque pour file de message.    */
#include <sys/ipc.h>     /* Bibliotheque pour gestion de clé.     */
#include <unistd.h>      /* Bibliothèque pour sleep.              */
#include "msq-washer.h"  /* Fichier pour fonctions de washer.     */

/**
  * Ensemble de macros pour les affichages.
*/
#define USAGE_HELP_HEADER\
    "\t-h, --help\n"\
    "\t\tdisplay this help and exit\n"\
    "\t-i, --key-proj-id=PROJ_ID\n"\
    "\t\tset the key project identifier to PROJ_ID (the default value is \"1\")"\

#define USAGE_HELP_FOOTER \
    "\t-p, --key-pathname=PATHNAME\n"\
    "\t\tset the key pathname to PATHNAME (the default value is \"file.ftok\")\n"\
    "\t-s, --seconds=SECONDS\n"\
    "\t\tset the seconds between each try (the default value is \"1\", a value less than or "\
    "equal to 0 enables the interactive mode where the input stream is read)\n"\
    "\t-t, --tries=TRIES\n"\
    "\t\tset the number of tries to TRIES (the default value is \"-1\", a negative value means"\
    " repeat for ever)\n"\
    "\t-v, --version\n"\
    "\t\toutput version information and exit\n"

#define USAGE_FOOTER\
    "Report bugs to Olivier Baconnais <o.baconnais@etud.univ-pau.fr> and Roman Badanin <r.badanin@etud.univ-pau.fr>."

#define USAGE_VERSION\
    "msq-server 20210323\n\n"\
    "Copyright (C) 2021 Olivier Baconnais and Roman Badanin.\n\n"\
    "Written by Olivier Baconnais <o.baconnais@etud.univ-pau.fr> and Roman Badanin <o.baconnais@etud.univ-pau.fr>."

#define USAGE_VERSION_CLIENT\
    "msq-client 20210323\n\n"\
    "Copyright (C) 2021 Olivier Baconnais and Roman Badanin.\n\n"\
    "Written by Olivier Baconnais <o.baconnais@etud.univ-pau.fr> and Roman Badanin <r.badanin@etud.univ-pau.fr>."

/**
  * Affichage de l'en-tête présentation.
*/
void affichage_USAGE(const int, const char*);

/**
 * Affichage de l'option version/help.
*/
void HelpVersionCommand(const int, const char*);

/**
  * Affichage de l'option help du serveur. *
*/
void affichage_Server();

/**
  * Affichage de l'option help du client.
*/
void affichage_Client();

/**
  * Fonction traitement message.
*/
int Msg_traitement(const msq_washer_t, const int);

#endif
