/**
  * Fichier msq-washer.c
*/
#include <stdio.h>      /* Bibliotheque standard.                         */
#include <stdlib.h>     /* Bibliotheque standard.                         */
#include <string.h>     /* Bibliotheque standard.                         */
#include <time.h>       /* Bibliothèque pour traitement de l'heure.       */
#include "msq-washer.h" /* Fichier pour fonctions washer.                 */

/**
  * Sérialise.
*/
void msq_washer_serialize(msq_washer_t washer, msq_washer_message_t *message)
{
    int i = 0, j = 0;
    int long_model = strlen(washer.model);
    int long_brand = strlen(washer.brand);

    message->type = 1;
    for (; i < long_brand; i++) {
        message->text[i] = washer.brand[i];
    }

    message->text[i] = '|';
    i++;

    for (; j < long_model; j++) {
        message->text[i] = washer.model[j];
        i++;
    }
}

/**
  * Désérialise.
*/
void msq_washer_deserialize(msq_washer_t *washer, msq_washer_message_t message)
{
    int i = 0, j = 0;
    int long_text = strlen(message.text);

    while (message.text[i] != '|') {
        i++;
    }

    for( ; j < i; j++) {
        /**
          * On récupère la marque.
        */
        washer->brand[j] = message.text[j];
    }
    /**
      * Rajout de la séquence de fin.
    */
    washer->brand[j] = '\0';

    j = i + 1;
    i = 0;

    for( ; j<long_text; j++) {
        washer->model[i] = message.text[j];
        i++;
    }
}

/**
  * Affiche la machine.
*/
void msq_washer_print(msq_washer_t washer) {
    time_t now;
    int year, month, day, hour, min, sec;
    struct tm* h_local = NULL;
    time(&now);
    h_local = localtime(&now);
    year    = h_local->tm_year + 1900;
    month   = h_local->tm_mon + 1;
    day     = h_local->tm_mday;
    hour    = h_local->tm_hour;
    min     = h_local->tm_min;
    sec     = h_local->tm_sec;
    printf("%02d-%02d-%02d %02d:%02d:%02d: %s: %s\n", year, month, day, hour, min, sec, washer.brand, washer.model);
}

/**
  * Définit la marque de la machine.
*/
int msq_washer_set_brand(msq_washer_t *washer, const char *brand) {
    if (strlen(brand) > MSQ_WASHER_BRAND_SIZE) {
        return msq_washer_error_brand;
    }
    else {
        strcpy(washer->brand, brand);
    }
    return 0;
}

/**
  * Définit le modèle de la machine.
*/
int msq_washer_set_model(msq_washer_t *washer, const char *model) {
    if (strlen(model) > MSQ_WASHER_MODEL_SIZE) {
        return msq_washer_error_model;
    }
    else {
      strcpy(washer->model, model);
    }
    return 0;
}
