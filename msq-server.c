/**
  * Fichier msq-server.c
*/
#include "msq-common.h" /* Fichier pour fonction common.  */
#include "msq-washer.h" /* Fichier pour fonction machine. */


int main (int argc, char* argv[]) {

    /*########################################################################################*/
    /*################################# Déclaration des variables. ###########################*/
    /*########################################################################################*/

    /**
      * Structure pour décrire les options possibles.
    */
    static struct option long_options[] = {
        { "help",         no_argument,       0, 'h' },
        { "key-proj-id",  required_argument, 0, 'i' },
        { "key-pathname", required_argument, 0, 'p' },
        { "seconds",      required_argument, 0, 's' },
        { "tries",        required_argument, 0, 't' },
        { "version",      no_argument,       0, 'v' },
        { 0,              0,                 0,  0  }
    };

    short hv                    = 0;            /* Variable pour mixiter -h -v.               */
    int opt                     = -1;           /* Variable pour le traitement des options.   */
    int msqid                   = -1;           /* Variable pour création de la file message. */
    long Proj_ID                = 1;            /* Variable pour ID.                          */
    long sl_time                = 1;            /* Variable pour taux de rafraichissement.    */
    long Tries                  = -1;           /* Variable pour le nombre d'essai.           */
    char* endptr                = NULL;         /* Pour lecture optarg.                       */
    char* PathName              = "file.ftok";  /* Variable pour chemin fichier ftok.         */
    key_t key                   = -1;           /* Variable pour clé file de message.         */
    msq_washer_t washer;                        /* Structure pour stockage informations.      */
    msq_washer_message_t buf;                   /* Buffer de lecture message file.            */

    /*########################################################################################*/
    /*################################### Traitement des options. ############################*/
    /*########################################################################################*/

    while ((opt = getopt_long(argc, argv, "hi:p:s:t:v", long_options, NULL)) != -1) {
        switch (opt) {
            case 'h': /* Affichage de l'aide. */
                HelpVersionCommand(1, "server");
                hv++;
                break;
            case 'i': /* Configuration de l'ID de la file de message. */
                Proj_ID = strtol(optarg, &endptr, 10);
                break;
            case 'p': /* Configuration du chemin pour fichier ftok. */
                PathName = optarg;
                break;
            case 's': /* Configuration du taux de rafraichissement. */
                sl_time = strtol(optarg, &endptr, 10);
                break;
            case 't': /* Configuration du nombre d'essai(s). */
                Tries = strtol(optarg, &endptr, 10);
                break;
            case 'v': /* Affichage de la version. */
                HelpVersionCommand(0, "server");
                hv++;
            break;
            default:
                break;
        }
    };

    if (hv != 0) {
        return 0;
    }

    /**
      * Affichage de l'ID et du chemin.
    */
    affichage_USAGE(Proj_ID, PathName);

    /*########################################################################################*/
    /*############################ Création de la file de message. ###########################*/
    /*########################################################################################*/

    /**
      * Création d'une clé pour la file de message.
    */
    if ((key = ftok(PathName, Proj_ID)) == -1) {
        fprintf(stderr, "./msq-server.out:%s:%d: Unable to create the System V IPC key from"
                        "the \"%s\" pathname and the \"%ld\" project identifier.\n"
                        , __FILE__, __LINE__, PathName, Proj_ID);
        return 1;
    }

    /**
      * Création de la file de message.
    */
    if ((msqid = msgget(key, IPC_CREAT | 0600 | IPC_EXCL)) == -1) {
        fprintf(stderr, "./msq-server.out:%s:%d: Unable to get the identifier of the System V"
                        "message queue from the \"0x%08x\" key.\n", __FILE__, __LINE__, key);
        return 1;
    }

    /*########################################################################################*/
    /*########################## Traitement de la file de message. ###########################*/
    /*########################################################################################*/

    while (Tries != 0)
    {
        /**
          * Mode intéractif.
        */
        if (sl_time <= 0)
        {
            printf("Press the Enter key to continue...");
            while (getchar()!='\n');
            /**
              * réception message dans la file, si pas de message le processus n'est pas bloqué *
            */
            if ((msgrcv(msqid, &buf, sizeof(msq_washer_message_t), 0, IPC_NOWAIT)) != -1) {
                msq_washer_deserialize(&washer,buf);
                msq_washer_print(washer);
            }
        }
        /**
          * Mode non-intéractif.
        */
        else
        {
            if ((msgrcv(msqid, &buf, sizeof(msq_washer_message_t), 0, IPC_NOWAIT)) != -1) {
                msq_washer_deserialize(&washer,buf);
                msq_washer_print(washer);
            }
            sleep(sl_time);
        }

        if (Tries > 0) {
            Tries--;
        }
    }

    /**
      * Suppression de la file.
    */
    if (msgctl(msqid, IPC_RMID, NULL) == -1)
        return 1;

    return 0;
}
