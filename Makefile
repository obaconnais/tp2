CC = gcc
CFLAGS = -std=c89 -pedantic -Wall -Werror -g -D_GNU_SOURCE
RM = rm -fv

.PHONY: all clean

all: msq-client.out msq-server.out

msq-common.o: msq-common.c msq-common.h
	$(CC) $(CFLAGS) -c -o $@ $<

msq-washer.o: msq-washer.c msq-washer.h
	$(CC) $(CFLAGS) -c -o $@ $<

msq-client.out: msq-client.c msq-common.o msq-washer.o
	$(CC) $(CFLAGS) -o $@ $^

msq-server.out: msq-server.c msq-common.o msq-washer.o
	$(CC) $(CFLAGS) -o $@ $^

clean:
	$(RM) *.o *.out
