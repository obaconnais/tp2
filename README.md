Fonction autorisées:

- [x] - 1 int fprintf(FILE *stream, const char *format, ...);
- [x] - 2 key_t ftok(const char *pathname, int proj_id);
- [x] - 3 int getchar(void);
- [x] - 4 int getopt_long(int argc, char * const argv[], const char *optstring, const struct option *longopts, int *longindex);
- [x] - 5 struct tm *localtime(const time_t *timep);
- [x] - 6 int msgctl(int msqid, int cmd, struct msqid_ds *buf);
- [x] - 7 int msgget(key_t key, int msgflg);
- [x] - 8 ssize_t msgrcv(int msqid, void *msgp, size_t msgsz, long msgtyp, int msgflg);
- [x] - 9 int msgsnd(int msqid, const void *msgp, size_t msgsz, int msgflg);
- [x] - 10 int printf(const char *format, ...);
- [x] - 11 unsigned int sleep(unsigned int seconds);
- [ ] - 12 char *strcpy(char *dest, const char *src);
- [ ] - 13 size_t strlen(const char *s);
- [ ] - 14 long int strtol(const char *nptr, char **endptr, int base);
- [x] - 15 time_t time(time_t *t);
