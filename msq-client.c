/**
  * Fichier mqs-client.c
*/
#include "msq-common.h" /* Fichier pour fonction common.  */
#include "msq-washer.h" /* Fichier pour fonction machine. */

int main (int argc, char* argv[])
{
    /*########################################################################################*/
    /*################################# Déclaration des variables. ###########################*/
    /*########################################################################################*/

    /**
      * Structure pour décrire les options possibles. *
    */
    static struct option long_options[] = {
        { "help",         no_argument,       0, 'h' },
        { "key-proj-id",  required_argument, 0, 'i' },
        { "key-pathname", required_argument, 0, 'p' },
        { "seconds",      required_argument, 0, 's' },
        { "tries",        required_argument, 0, 't' },
        { "version",      no_argument,       0, 'v' },
        { "washer-brand", required_argument, 0, 'b' },
        { "washer-model", required_argument, 0, 'm' },
        { 0,              0,                 0,  0  }
    };

    int opt                  = -1;            /* Variable pour choix des options.           */
    short hv                 = 0;             /* Variable pour mixiter -h -v.               */
    int msgid                = -1;            /* Variable pour création de la file message. */
    long sl_time             = 1;             /* Variable taux de rafraichissement.         */
    long Proj_ID             = 1;             /* Variable pour ID.                          */
    long Tries               = 1;             /* Variable pour le nombre d'essai.           */
    char* PathName           = "file.ftok";   /* Variable pour chemin fichier ftok.         */
    char* endptr             = NULL;          /* Pour lecture optarg.                       */
    key_t key                = -1;            /* Variable pour clé file de message.         */
    msq_washer_t  washer;                     /* Structure pour stockage.                   */

    /*######################################################################################*/
    /*############################# Initialisation variables. ##############################*/
    /*######################################################################################*/

    msq_washer_set_brand(&washer, "Default brand");
    msq_washer_set_model(&washer, "Default model");

    /*######################################################################################*/
    /*################################# Traitement des options. ############################*/
    /*######################################################################################*/

    while ((opt = getopt_long(argc, argv, "hi:p:s:t:vb:m:", long_options, NULL)) != -1) {
        switch (opt) {
            case 'b': /* Configuration de la marque de la machine. */
                if(msq_washer_set_brand(&washer, optarg) == -2){
                  fprintf(stderr, "./msq-client.out:%s:%d: Unable to set the washer"
                                  "brand to \"%s\".\n", __FILE__, __LINE__, optarg);
                  return 1;
                }
                break;
            case 'm': /* Configuration du modèle de la machine. */
                if(msq_washer_set_model(&washer, optarg) == -1){
                  fprintf(stderr, "./msq-client.out:%s:%d: Unable to set the washer model"
                                  "to \"%s\".\n", __FILE__, __LINE__, optarg);
                  return 1;
                }
                break;
            case 'h': /* Affichage de l'aide. */
                HelpVersionCommand(1, "client");
                hv++;
                break;
            case 'i': /* Configuration de l'ID de la file de message. */
                Proj_ID = strtol(optarg, &endptr, 10);
                break;
            case 'p': /* Configuration du chemin pour fichier ftok. */
                PathName = optarg;
                break;
            case 's': /* Configuration du taux de rafraichissement. */
                sl_time = strtol(optarg, &endptr, 10);
                break;
            case 't': /* Configuration du nombre d'essai(s). */
                Tries = strtol(optarg, &endptr, 10);
                break;
            case 'v': /* Affichage de la version. */
                HelpVersionCommand(0, "client");
                hv++;
            break;
            default:
                break;
        }
    };

    if (hv != 0) {
        return 0;
    }

    /**
      * Affichage de l'ID et du chemin.
    */
    affichage_USAGE(Proj_ID, PathName);

    /**
      * Création d'une clé pour la file de message.
    */
    if ((key = ftok(PathName, Proj_ID)) == -1) {
        fprintf(stderr, "./msq-client.out:%s:%d: Unable to create the System V IPC "
                        "key from the \"%s\" pathname and the \"%ld\" project identifier."
                        "\n", __FILE__, __LINE__, PathName, Proj_ID);
        return 1;
    }

    /**
      * On obtient une file de message à partir d’une clé.
    */
    if ((msgid = msgget(key, IPC_EXCL)) == -1) {
        fprintf(stderr, "./msq-client.out:%s:%d: Unable to get the identifier of the "
                        "System V message queue from the \"0x%08x\" key.\n", __FILE__, __LINE__, key);
        return 1;
    }

    /**
      * Traitement du(des) message(s) dans la file.
    */
    while(Tries != 0) {
        if (sl_time > 0) { /* Mode intéractif. */
            Msg_traitement(washer, msgid);
            sleep(sl_time);
        }
        else { /* Mode non intéractif. */
            printf("Press the Enter key to continue...");
            while (getchar() != '\n');
            Msg_traitement(washer, msgid);
        }
        if (Tries > 0) {
            Tries--;
        }
    }
    return 0;
}
