/**
  * Fichier msq-common.c
*/
#include "msq-common.h" /* Fichier signatures fonctions msq-common.c

/**
  * Affichage de l'option aide du serveur.
*/
void affichage_Server() {
    printf("Usage: ./msq-server.out [OPTION]...\n");
    printf("Receive washers from clients through a message queue.\n\n");
    printf("Options:\n");
    printf("%s\n", USAGE_HELP_HEADER);
    printf("%s\n", USAGE_HELP_FOOTER);
    printf("%s\n", USAGE_FOOTER);
}

/**
  * Affichage de l'option help du client.
*/
void affichage_Client() {
    printf("Usage: ./shm-client.out [OPTION]...\n");
    printf("Send a washer to a server through shared memory.\n\n");
    printf("Options:\n");
    printf("\t-b, --washer-brand=BRAND\n");
    printf("\t\tset the washer brand to BRAND (the default value is \"Default brand\")\n");
    printf("%s", USAGE_HELP_HEADER);
    printf("\n\t-m, --washer-model=MODEL\n");
    printf("\t\tset the washer model to MODEL (the default value is \"Default model\")\n");
    printf("%s\n", USAGE_HELP_FOOTER);
    printf("%s\n", USAGE_FOOTER);
}

/**
  * Affichage de l'en-tête de présentation.
*/
void affichage_USAGE(const int proj_id, const char* pathname) {
    printf("proj_id = \"%d\"\npathname = \"%s\"\n", proj_id, pathname);
}

/**
 * Affichage de l'option version/aide.
*/
void HelpVersionCommand(const int help, const char* nom) {
    /**
      * Affiche aide.
    */
    if (help == 1) {
        if (strcmp(nom, "server") == 0)
            affichage_Server();
        else
            affichage_Client();
    }
    /**
      * Affiche version.
    */
    else {
        if (strcmp(nom, "server") == 0)
            printf("%s\n", USAGE_VERSION);
        else
            printf("%s\n", USAGE_VERSION_CLIENT);
    }
}

/**
  * Fonction traitement des messages.
*/
int Msg_traitement(const msq_washer_t washer, const int msgid)
{
    /**
      * Variable pour envoi message.
    */
    msq_washer_message_t msg;

    /**
      * Sérialisation du message avant envoi.
    */
    msq_washer_serialize(washer, &msg);

    /**
      * Envoie du message dans la file de messages.
    */
    if ((msgsnd(msgid, &msg, sizeof(msq_washer_message_t), 0)) == -1) {
        fprintf(stderr, "./msq-common.c:%s:%d: Unable to send message to serber.\n", __FILE__, __LINE__);
        return 1;
    }

    /**
      * Affichage de la machine.
    */
    msq_washer_print(washer);

    return 0;
}
